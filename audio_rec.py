from math import tanh, exp
from math import ceil
from time import time

from simplification.cutil import simplify_coords
from scipy.io.wavfile import read, write
import matplotlib.pyplot as plt
import numpy as np
import wave
from rdp import rdp
from collections import Counter

from knearest import Brain


brain_name = 'audio_brain.sav'

SAMPLE_RATE = 0
# SPLITS = 2500
INPUTS = 100
IDX = 0


def open_wav(filename):
    """Opening wav file to array between (-1, 1). If wav is sterio convert to mono"""
    global SAMPLE_RATE
    print("Reading wav file... ")
    ifile = read(filename)
    SAMPLE_RATE = ifile[0]
    wav_data = ifile[1]
    print('File Sample Rate: ', SAMPLE_RATE)
    if isinstance(ifile[1][0], np.ndarray):
        audiodata = ifile[1].astype(float)
        wav_data = audiodata.sum(axis=1) / 2
    return wav_data


def sigmoid(x):
  return 1 / (1 + exp(-x))


def normalize_array(array):
    """Returns the array where every element is between (-1, 1)."""
    return array / max(max(array), abs(min(array)))


def partition_array(array, pieces=500): #NOT USED
    """Splits 1D array into equal sized arrays of n size."""
    array_len = len(array)
    array_pieces = (array_len // pieces)
    return np.array_split(array, array_pieces)


def reduce_2darray(array, epsilon=0.01): # NOT USED
    """Reduces an array using the RDP alogrithm. returning a pattern that represents the same data with fewer points"""
    reduced_2d_array = []
    for array_sector in array:
        two_d_array_sector = []
        idx = 0
        for s in array_sector:
            two_d_array_sector.append([idx, s])
            idx += 1
        simplified_2d_sector = simplify_coords(two_d_array_sector, epsilon)
        reduced_2d_array.append(simplified_2d_sector)
    
    reduced_array = []
    for sector in reduced_2d_array:
        reduced_sector = []
        for coord in sector:
            reduced_sector.append(coord[1]) 
        reduced_array.append(reduced_sector)
    return reduced_array


def reduce_array(array_sector, epsilon=0.01):
    """Reduces an array using the RDP alogrithm. returning a pattern that represents the same data with fewer points"""
    reduced_2d_array = []
    two_d_array_sector = []
    idx = 0
    for s in array_sector:
        two_d_array_sector.append([idx, s])
        idx += 1
    simplified_2d_sector = rdp(two_d_array_sector, epsilon=epsilon)
    # simplified_2d_sector = simplify_coords(two_d_array_sector, epsilon)
    reduced_2d_array.append(simplified_2d_sector)
    reduced_array = []
    for sector in reduced_2d_array:
        reduced_sector = []
        for coord in sector:
            reduced_sector.append(coord[1]) 
        reduced_array.append(reduced_sector)
    return reduced_array


def do_audio_shit(): # NOTUSED
    """Opening wav file to array between (-1, 1)"""

    audio_data = open_wav(input_audio)
    audio_normalised = normalize_array(audio_data)
    partitioned_array = partition_array(audio_normalised, SPLITS)
    reduced_array = reduce_2darray(partitioned_array, EPSILON)
    assert len(partitioned_array) == len(reduced_array), \
        f"Expected partitioned array to be the same size as the reduced array"
    partitioned_array = partition_array(audio_data, SPLITS)
    return reduced_array, partitioned_array


def extend_array(array, desired_length) -> list:
    ele = ceil(desired_length / (len(array) - 1))
    expanded_array = []
    for i, v in enumerate(array):
        if i > 0:
            if len(expanded_array) > 0:
                expanded_array = expanded_array[:-1]
            expanded_array.extend(np.linspace(array[i - 1], array[i], ele))
    return expanded_array


def extend_array2(original_array, reduced_array):
    """
    line form
    """
    def gradient(x1, x2, y1, y2):
        return (y2 - y1) / (x2 - x1)
    def solve_for_c(x, y, m):
        return y / (x * m)
    new_array = []
    
    for r in reduced_array:
        pass


def round_and_remove_most_common(array):
    rounded_array = [round(num) for num in array]
    counter = Counter(rounded_array)
    most_common = counter.most_common(1)[0][0]
    result = [num for num in array if round(num) != most_common]

    return result


def things(PREDICT: bool, LOAD_BRAIN: bool = False):
    global IDX, INPUTS, INPUT_AUDIO
    
    outputs = open_wav(INPUT_AUDIO)
    norm_outputs = normalize_array(outputs)

    print("Doing Brain stuff... ")
    brain = Brain(INPUTS, 1)
    if PREDICT or LOAD_BRAIN:
        print("Loading brain")
        brain.load_brain(brain_name)
    else:
        print("DOING TRAINING")
    
    pn = -1
    pt = time()
    bt = time()
    
    predictions = []

    IDX = 0
    while IDX + INPUTS < len(outputs):

        n = round(((IDX + INPUTS)/len(outputs)) * 100)
        if n != pn:
            time_step = round(time() - pt, 1)
            time_total = round(time()- bt)
            time_rem = round((100 / (n + 1)) * time_total)
            print(f"\rProgress {n}% {time_step}s - {time_total // 60}:{time_total % 60:02} : {time_rem // 60}:{time_rem % 60:02} total\t{IDX} / {len(outputs)}", end='')
            pn = n
            pt = time()

        output = outputs[IDX:IDX + INPUTS]
        norm_output = norm_outputs[IDX:IDX + INPUTS]
        # norm_output = round_and_remove_most_common(norm_output)
        
        # epsilon = 0.01 # max(norm_output) / 10
        reduced_inputs = reduce_array(norm_output, epsilon=20)[0]

        # reduced_inputs = round_and_remove_most_common(reduced_inputs)
        # from pdb import set_trace
        # set_trace()
        # reduced_inputs = [e for e in reduced_inputs if round(e) != b]
        
        if len(reduced_inputs) < 4:
            reduced_inputs = [0] * INPUTS
        extended_inputs = extend_array(reduced_inputs, INPUTS)
        
        # print(IDX, len(reduced_inputs))
        # print([round(e, 2) for e in reduced_inputs])
        # print([round(e, 2) for e in extended_inputs])
        # plt.plot(output)
        # plt.plot(reduced_inputs)
        # plt.plot(extended_inputs)
        # plt.show()
        # plt.show()
        # print(len(norm_output))
        # exit()
        # # if max(extended_inputs) > 0:
        # print()
        # print(output)
        # print(reduced_inputs[:50])
        # for i, e in enumerate(reduced_inputs):
        #     print(e, ' ', end='')
        #     if i > 50:
        #         break
        # print()
        # print()
        # print(extended_inputs)

        if not PREDICT:
            brain.add(extended_inputs, output)
            # if max(output) < 0.5:
            #     print("skipped")
            # else:
            #     brain.add(extended_inputs, output)
        else:
            predictions.extend(brain.predict(extended_inputs))
        IDX += INPUTS
    predictions = np.array(predictions, dtype=np.int16)

    try:
        if not PREDICT:
            print("Saving brain")
            brain.save_brain(brain_name)
        else:
            write('output.wav', int(SAMPLE_RATE), predictions)
    except Exception as e:
        print("ERROR!", e)
        breakpoint()
    print("Done.")


if __name__ == '__main__':
    INPUT_AUDIO = 'audio_samples/bible_reading.wav'
    # INPUT_AUDIO = 'audio_samples/CliQ1.wav'
    things(False)
    # INPUT_AUDIO = 'audio_samples/CliQ2.wav'
    # things(False, True)
    INPUT_AUDIO = 'audio_samples/0.wav'
    # INPUT_AUDIO = 'audio_samples/CliQ1.wav'
    things(True)
# INPUT_AUDIO = 'audio_samples/cliqlong.wav'
# things(False, True)
# # breakpoint()


# inputs = open_wav(input_audio)
# print('Plotting')
# # breakpoint()
# plt.plot(inputs[IDX:IDX + INPUTS])
# plt.show()
# # breakpoint()
# arr = reduce_array(inputs[IDX:IDX + INPUTS], max(inputs) / 1000)
# plt.plot(arr[0])
# plt.show()

# data = np.random.uniform(-1,1,44100) # 44100 random samples between -1 and 1
# scaled = np.int16(data/np.max(np.abs(data)) * 32767)



# def train():
#     global IDX, INPUTS, INPUT_AUDIO
#     print("DOING TRAINING")
#     print("Doing Brain stuff... ")
    
#     outputs = open_wav(INPUT_AUDIO)

#     b = Brain(INPUTS, 1)
#     pn = -1
#     pt = time()
#     bt = time()
#     IDX = 0
#     while IDX + INPUTS < len(outputs):
#         n = round(((IDX + INPUTS)/len(outputs)) * 100)
#         if n != pn:
#             print(f"Progress {n}% {round(time() - pt, 1)}s - {round(time()-bt, 1)}s total\t{IDX} / {len(outputs)}")
            
#             pn = n
#             pt = time()

#         output = outputs[IDX:IDX + INPUTS]
#         norm_output = normalize_array(output)
#         epsilon = max(output) / 1000
#         inputs = reduce_array(norm_output, epsilon=epsilon)[0]
#         inputs = extend_array(inputs, INPUTS)
#         b.add(inputs, output)
#         IDX += INPUTS

#     print("Saving brain")
#     b.save_brain(brain_name)
#     print("Done.")

# def predict():
#     global IDX, INPUTS, INPUT_AUDIO
#     print("Loading brain")
#     b = Brain(0, 1)
#     b.load_brain(brain_name)
#     print("Done.")

#     print("DOING PREDICTIONS")
#     print("Doing Brain stuff... ")
#     print("Doing Predictions... ", end='')
#     pn = -1
#     pt = time()
#     bt = time()
#     predictions = []
#     outputs = open_wav(INPUT_AUDIO)
#     IDX = 0
#     while IDX + INPUTS < len(outputs):
#         n = round(((IDX + INPUTS)/len(outputs)) * 100)
#         if n != pn:
#             print(f"Progress {n}% {round(time() - pt, 1)}s - {round(time()-bt, 1)}s total")
#             pn = n
#             pt = time()
#         output = outputs[IDX:IDX + INPUTS]
#         epsilon = max(output) / 1000
#         norm_output = normalize_array(output)
#         inputs = reduce_array(norm_output, epsilon=epsilon)[0]
#         inputs = extend_array(inputs, INPUTS)
#         predictions.extend(b.predict(inputs).tolist())
#         IDX += INPUTS
#     predictions = np.array(predictions, dtype=np.int16)

#     print("Done")

#     try:
#         write('output.wav', int(SAMPLE_RATE), predictions)
#     except Exception as e:
#         print("error!", e)
#         breakpoint()
#     print("Done.")
