from _thread import start_new_thread, _count
from time import sleep

# from sklearn.ensemble import RandomForestRegressor
# from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

mndata = MNIST('./data')
mndata.select_emnist('letters')
images, labels = mndata.load_training()

"""
balanced
byclass
bymerge
digits
letters
mnist
"""

regressor = KNeighborsClassifier(3)

# print(f"Training brain with '{train_range_2 - train_range_1}' values")
a = []
b = []
print("Training:")
regressor.fit(images[:len(images)], labels[:len(images)])

images, labels = mndata.load_testing()

predict_range_1 = 0
predict_range_2 = len(images)

print(f"Predicting '{predict_range_2 - predict_range_1}' values")

incorrect_values = []

correct = 0
count = 0
# breakpoint()

def guess(index):
    global correct, count, images, labels, brain, incorrect_values
    try:
        prediction = round(regressor.predict([images[index]])[0])

        correct += 1 if prediction == labels[index] else 0
        COLOUR = "O" if prediction == labels[index] else "X"
        if prediction != labels[index]:
            incorrect_values.append(index)
        count += 1
        print(f"{COLOUR} Prediction {index}: '{chr(65 + prediction)}' correct '{chr(65 + labels[index])}'\t\t{round(correct/count* 100)}% {correct}/{count}")
    except Exception as e:
        print(f'Error! {e}')

for i in range(predict_range_1, predict_range_2):
    waits = 0
    while _count() > 30 and waits < 10:
        waits += 1
        sleep(5)
    start_new_thread(guess, (i, ))
    sleep(1)

print("Prediction Completed!")
print(f"Accuracy {correct}/{predict_range_2 - predict_range_1}")

while _count() > 0:
    pass

f = open('errors', 'w+')
f.write(str(incorrect_values).replace(' ', '\n'))
f.close()
