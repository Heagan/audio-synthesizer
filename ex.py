import numpy as np

def expand_array(arr, target_length):
    if target_length <= len(arr):
        return arr

    expanded_array = np.linspace(arr[0], arr[-1], target_length)

    return expanded_array

# Example usage
original_array = [0, 9, 5]
target_length = 20
target = 7


result = expand_array(original_array, target_length)

print(result)
