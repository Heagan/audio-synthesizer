from _thread import start_new_thread, _count
from time import sleep
from mnist import MNIST
from knearest import Brain

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

brain = Brain(784, 1)

mndata = MNIST('./datafiles')
images, labels = mndata.load_training()

train_range_1 = 0
train_range_2 = 6000

print(f"Training brain with '{train_range_2 - train_range_1}' values")
for i in range(train_range_1, train_range_2):
    brain.add(images[i], labels[i])
print("Training Done")

brain.save_brain("brain.sav")

predict_range_1 = 6001
predict_range_2 = 10000

print(f"Predicting '{predict_range_2 - predict_range_1}' values")

incorrect_values = []

correct = 0
count = 0

def guess(index, ind):
    global correct, count, images, labels, brain, incorrect_values
    try:
        # brain = Brain(784, 2)
        # brain.load_brain("brain.sav")
        prediction = round(brain.predict(images[index]))
        correct += 1 if prediction == labels[index] else 0
        COLOUR = GREEN if prediction == labels[index] else RED
        if prediction == labels[index]:
            brain.add(images[index], labels[index])
        else:
            incorrect_values.append(index)
        count += 1
        print(f"{COLOUR}Prediction {index}: '{prediction}' correct '{labels[index]}'\t\t{round(correct/count* 100)}% {correct}/{count}{NC}")
    except Exception as e:
        print(f'Error! {e}')


for i in range(predict_range_1, predict_range_2):
    waits = 0
    while _count() > 30 and waits < 10:
        waits += 1
        sleep(5)
    # print(f"Creating thread: {_count()}")
    start_new_thread(guess, (i, i))
    sleep(1)
    

print("Prediction Completed!")
print(f"Accuracy {correct}/{predict_range_2 - predict_range_1}")

# breakpoint()
f = open('errors', 'w+')
f.write(str(incorrect_values).replace(' ', '\n'))
f.close()
while _count() > 0:
    pass
